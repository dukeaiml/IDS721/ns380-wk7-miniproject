# ns380-wk7-miniproject

This project utilizes a vector database to search for similar NBA players over the past 5 seasons using their statistics as vectors.

The specific vector database used is the Qdrant database because it is open source and can be hosted locally.

### Usage

First you have to start the qdrant server using a command like
```
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

This will look like this
![alt text](image.png)

Next, you can run this project simply using
```
cargo run
```

You will see an output like
```
Player: "Avery Bradley 2022"
Player: "Austin Rivers 2022"
Player: "Luguentz Dort 2020"
Player: "Isaiah Livers 2023"
Player: "Cam Reddish 2024"
Player: "Wesley Matthews 2022"
Player: "Wesley Matthews 2020"
Player: "Isaiah Livers 2024"
Player: "Ochai Agbaji 2024"
Player: "Adam Mokoka 2020"
```

If you want to search for similarity to a different player (current player is Avery Bradley from 2022), you can change the name on line 78. This should change the output to the players with the most similar stats to the new player you provide.

Here is a screenshot of the code running
![alt text](image-1.png)
