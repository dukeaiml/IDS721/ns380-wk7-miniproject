use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
    CreateCollection, SearchPoints, VectorParams, VectorsConfig,
};
use serde_json::json;
use polars::prelude::*;

#[tokio::main]
async fn main() -> Result<()> {
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    let collection_name = "nba-stats";
    client.delete_collection(collection_name).await?;

    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 12,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    let mut payloads = Vec::new();
    let mut vectors = Vec::new();
    let mut vector_against = vec![0.;12];
    for year in 2020..2025 {
        let filename = format!("data/stats-{}.csv", year);
        let q = LazyCsvReader::new(filename)
            .has_header(true)
            .finish()?
            .select([col("Player"), col("G"), col("MP"), col("FGA"), col("FG%"), col("3PA"), col("3P%"), col("eFG%"), col("TRB"), col("AST"), col("STL"), col("BLK"), col("TOV"), col("PTS")]);

        let df = q.collect()?.unique(Some(&["Player".to_string()]), UniqueKeepStrategy::First, None)?;
        
        for i in 0..df.height() {
            let row = df.get_row(i)?.0;
            let payload: Payload = json!(
                {
                    "Player": format!("{} {}", row[0].to_string().replace("\"", ""), year),
                    "G": row[1].to_string().parse::<u32>().unwrap(),
                    "MP": row[2].to_string().parse::<f32>().unwrap(),
                    "FGA": row[3].to_string().parse::<f32>().unwrap(),
                    "FG%": row[4].to_string().parse::<f32>().unwrap_or(0.0),
                    "3PA": row[5].to_string().parse::<f32>().unwrap(),
                    "3P%": row[6].to_string().parse::<f32>().unwrap_or(0.0),
                    "eFG%": row[7].to_string().parse::<f32>().unwrap_or(0.0),
                    "TRB": row[8].to_string().parse::<f32>().unwrap(),
                    "AST": row[9].to_string().parse::<f32>().unwrap(),
                    "STL": row[10].to_string().parse::<f32>().unwrap(),
                    "BLK": row[11].to_string().parse::<f32>().unwrap(),
                    "TOV": row[12].to_string().parse::<f32>().unwrap(),
                    "PTS": row[13].to_string().parse::<f32>().unwrap()
                }
            ).try_into().unwrap();
            let vector = vec![
                row[2].to_string().parse::<f32>().unwrap(),
                row[3].to_string().parse::<f32>().unwrap(),
                row[4].to_string().parse::<f32>().unwrap_or(0.0),
                row[5].to_string().parse::<f32>().unwrap(),
                row[6].to_string().parse::<f32>().unwrap_or(0.0),
                row[7].to_string().parse::<f32>().unwrap_or(0.0),
                row[8].to_string().parse::<f32>().unwrap(),
                row[9].to_string().parse::<f32>().unwrap(),
                row[10].to_string().parse::<f32>().unwrap(),
                row[11].to_string().parse::<f32>().unwrap(),
                row[12].to_string().parse::<f32>().unwrap(),
                row[13].to_string().parse::<f32>().unwrap()
            ];
            payloads.push(payload);
            if format!("{} {}", row[0].to_string().replace("\"", ""), year) == "Avery Bradley 2022" {
                vector_against = vector.clone();
            }
            vectors.push(vector);
        }
    }

    let mut points = Vec::new();
    for i in 0..payloads.len() {
        points.push(PointStruct::new(i as u64, vectors[i].clone(), payloads[i].clone()));
    }

    client.upsert_points_blocking(collection_name, None, points, None).await?;

    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vector_against,
            limit: 10,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    let mut iter = search_result.result.into_iter();
    loop {
        let found_point = iter.next();
        if found_point.is_none() {
            break;
        }
        let mut payload = found_point.unwrap().payload;
        let name = payload.remove("Player").unwrap().to_string();
        println!("Player: {}", name);
    }

    Ok(())
}